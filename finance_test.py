import unittest
import finance
import finance.service
import os
import getpass

def make_client():
    if not os.environ.has_key('GOOGLE_USER'):
        raise Exception('"GOOGLE_USER not set')
    else:
        user = os.environ['GOOGLE_USER']
    if not os.environ.has_key('GOOGLE_PASSWORD'):
        password = getpass.getpass('Enter password for %s: ' %
                                   user)
    else:
        password = os.environ['GOOGLE_PASSWORD']

    client = finance.service.FinanceService()
    client.ClientLogin(user, password)
    return client


class TestSequenceFunctions(unittest.TestCase):
    user = None
    password = None

    def setUp(self):
        if not self.user:
            if not os.environ.has_key('GOOGLE_USER'):
                raise Exception('"GOOGLE_USER not set')
            else:
                self.user = os.environ['GOOGLE_USER']
        if not self.password:
            if not os.environ.has_key('GOOGLE_PASSWORD'):
                self.password = getpass.getpass('Enter password for %s: ' %
                                                self.user)
            else:
                self.password = os.environ['GOOGLE_PASSWORD']

        self.client = finance.service.FinanceService()
        self.client.ClientLogin(self.user, self.password)

    def testGetPortfolioFeed(self):
        feed = self.client.GetPortfolioListFeed()
        print feed

if __name__ == '__main__':
    unittest.main()
