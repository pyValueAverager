#
# Copyright (C) 2006 Google Inc.
# Copyright (C) 2008 David Euresti
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.


try:
    from xml.etree import cElementTree as ElementTree
except ImportError:
    try:
        import cElementTree as ElementTree
    except ImportError:
        from elementtree import ElementTree

import atom
import gdata

GFIN_NAMESPACE = 'http://schemas.google.com/finance/2007'

class PortfolioListEntry(gdata.GDataEntry):
    _tag = 'entry'
    _namespace = atom.ATOM_NAMESPACE
    _children = gdata.GDataEntry._children.copy()
    _attributes = gdata.GDataEntry._attributes.copy()


def PortfolioListEntryFromString(xml_string):
    """Converts an XML string into a PortfolioListEntry object.

    Args:
      xml_string: string The XML describing a Document List feed entry.

    Returns:
      A DocumentListEntry object corresponding to the given XML.
    """
    return atom.CreateClassFromXMLString(PortfolioListEntry,
                                         xml_string)

class PortfolioListFeed(gdata.GDataFeed):
    """A feed containing a list of Portfolio Items"""

    _tag = 'feed'
    _namespace = atom.ATOM_NAMESPACE
    _children = gdata.GDataFeed._children.copy()
    _attributes = gdata.GDataFeed._attributes.copy()
    _children['{%s}entry' % atom.ATOM_NAMESPACE] = ('entry',
                                                    [PortfolioListEntry])

def PortfolioListFeedFromString(xml_string):
    """Converts an XML string into a PortfolioListFeed object.

    Args:
      xml_string: string The XML describing a PortfolioList feed.

    Returns:
      A PortfolioListFeed object corresponding to the given XML.
    """
    return atom.CreateClassFromXMLString(PortfolioListFeed, xml_string)


class Symbol(atom.AtomBase):

    _tag = 'symbol'
    _namespace = GFIN_NAMESPACE
    _children = atom.AtomBase._children.copy()
    _attributes = atom.AtomBase._attributes.copy()
    _attributes['symbol'] = 'symbol'
    _attributes['fullName'] = 'fullName'

    def __init__(self, extension_elements=None, value=None, scope_type=None,
                 extension_attributes=None, text=None):
        self.value = value
        self.type = scope_type
        self.text = text
        self.extension_elements = extension_elements or []
        self.extension_attributes = extension_attributes or {}

class PositionData(atom.AtomBase):

    _tag = 'positionData'
    _namespace = GFIN_NAMESPACE
    _children = atom.AtomBase._children.copy()
    _attributes = atom.AtomBase._attributes.copy()
    _attributes['shares'] = 'shares'

    def __init__(self, extension_elements=None, value=None, scope_type=None,
                 extension_attributes=None, text=None):
        self.value = value
        self.type = scope_type
        self.text = text
        self.extension_elements = extension_elements or []
        self.extension_attributes = extension_attributes or {}


class PositionListEntry(gdata.GDataEntry):
    _tag = 'entry'
    _namespace = atom.ATOM_NAMESPACE
    _children = gdata.GDataEntry._children.copy()
    _attributes = gdata.GDataEntry._attributes.copy()

    _children['{%s}symbol' % GFIN_NAMESPACE] = ('symbol', Symbol)
    _children['{%s}positionData' % GFIN_NAMESPACE] = ('positionData',
                                                      PositionData)


def PositionListEntryFromString(xml_string):
    """Converts an XML string into a PositionListEntry object.

    Args:
      xml_string: string The XML describing a Position List feed entry.

    Returns:
      A PositionListEntry object corresponding to the given XML.
    """
    return atom.CreateClassFromXMLString(PositionListEntry, xml_string)



class PositionListFeed(gdata.GDataFeed):
    """A feed containing a list of Position Items"""

    _tag = 'feed'
    _namespace = atom.ATOM_NAMESPACE
    _children = gdata.GDataFeed._children.copy()
    _attributes = gdata.GDataFeed._attributes.copy()
    _children['{%s}entry' % atom.ATOM_NAMESPACE] = ('entry',
                                                    [PositionListEntry])


def PositionListFeedFromString(xml_string):
    """Converts an XML string into a PositionListFeed object.

    Args:
      xml_string: string The XML describing a PositionList feed.

    Returns:
      A PositionListFeed object corresponding to the given XML.
    """
    return atom.CreateClassFromXMLString(PositionListFeed, xml_string)
