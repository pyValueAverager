#
# Copyright (C) 2006 Google Inc.
# Copyright (C) 2008 David Euresti
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

import urllib
import atom.service
import gdata.service
import finance

class FinanceService(gdata.service.GDataService):

    def __init__(self, email=None, password=None, source=None,
                 server='finance.google.com', additional_headers=None):
        """Constructor for the FinanceService.

        Args:
          email: string (optional) The e-mail address of the account to use for
          authentication.
          password: string (optional) The password of the account to use for
                    authentication.
          source: string (optional) The name of the user's application.
          server: string (optional) The server the feed is hosted on.
          additional_headers: dict (optional) Any additional HTTP headers to be
                              transmitted to the service in the form of
                              key-value pairs.

        Yields:
          A FinanceService object used to communicate with the Google Finance
        service.
        """

        gdata.service.GDataService.__init__(
            self, email=email, password=password, service='finance',
            source=source, server=server,
            additional_headers=additional_headers)

    def QueryPortfolioListFeed(self, uri):
        """Retrieves a PortfolioListFeed by retrieving a URI based off the
        Portfolio List feed, including any query parameters. A PortfolioQuery
        object can be used to construct these parameters.

        Args:
          uri: string The URI of the feed being retrieved possibly with query
               parameters.

        Returns:
          A PortfolioListFeed object representing the feed returned by the
          server.
        """
        return self.Get(uri, converter=finance.PortfolioListFeedFromString)


    def GetPortfolioListEntry(self, uri):
        """Retrieves a particular DocumentListEntry by its unique URI.

        Args:
          uri: string The unique URI of an entry in a Document List feed.

        Returns:
          A DocumentListEntry object representing the retrieved entry.
        """
        return self.Get(uri, converter=finance.PortfolioListEntryFromString)


    def GetPortfolioListFeed(self):
        """Retrieves a feed containing all of a user's documents."""

        q = PortfolioQuery();
        return self.QueryPortfolioListFeed(q.ToUri())

    def QueryPositionListFeed(self, uri):
        """Retrieves a DocumentListFeed by retrieving a URI based off the
        Document List feed, including any query parameters. A DocumentQuery
        object can be used to construct these parameters.

        Args:
        uri: string The URI of the feed being retrieved possibly with query
        parameters.

        Returns:
        A DocumentListFeed object representing the feed returned by the server.
        """
        return self.Get(uri, converter=finance.PositionListFeedFromString)


    def GetPositionListFeed(self, port):
        uri = port.GetEditLink().href + '/positions'
        print uri
        return self.QueryPositionListFeed(uri)

class PortfolioQuery(gdata.service.Query):
    """Object used to construct a URI to query the Google Portfolio List feed
    """

    def __init__(self,feed='/finance/feeds/default', visibility='portfolios',
                 projection=None, text_query=None, params=None,
                 categories=None):

        """Constructor for Portfolio List Query

        Args:

          feed: string (optional) The path for the feed.

          visibility: string (optional) The visibility chosen for the current
                      feed.
          projection: string (optional) The projection chosen for the current
                      feed.

          text_query: string (optional) The contents of the q query
                      parameter. This string is URL escaped upon conversion to
                      a URI.

          params: dict (optional) Parameter value string pairs which become URL
                  params when translated to a URI. These parameters are added
                  to the query's items.

          categories: list (optional) List of category strings which should be
                      included as query categories. See gdata.service.Query for
                      additional documentation.

        Yields:
          A DocumentQuery object used to construct a URI based on the Document
          List feed.
        """

        self.visibility = visibility
        self.projection = projection
        gdata.service.Query.__init__(self, feed, text_query, params,
                                     categories)

    def ToUri(self):
        """Generates a URI from the query parameters set in the object.

        Returns:
          A string containing the URI used to retrieve entries from the
          Document List feed.
        """

        old_feed = self.feed
        if self.projection:
            self.feed = '/'.join([old_feed, self.visibility, self.projection])
        else:
            self.feed = '/'.join([old_feed, self.visibility])
        new_feed = gdata.service.Query.ToUri(self)
        self.feed = old_feed
        print 'feed = %s' % new_feed
        return new_feed


