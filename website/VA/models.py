from django.db import models

class Portfolio(models.Model):
    name = models.CharField(max_length=200)
    target = models.IntegerField()
    target_date = models.DateTimeField('')

    def __unicode__(self):
        return u"%s" % (self.name)

class Account(models.Model):
    name = models.CharField(max_length=200)
    portfolio = models.ForeignKey(Portfolio)

    def __unicode__(self):
        return u"%s" % (self.name)

class Investment(models.Model):
    name = models.CharField(max_length=200)
    ticker = models.CharField(max_length=200)

    def __unicode__(self):
        return u"%s" % (self.name)

class Position(models.Model):
    investment = models.ForeignKey(Investment)
    account = models.ForeignKey(Account)
    shares = models.FloatField()

    def __unicode__(self):
        return u"%s Shares of %s on %s" % (self.shares,
                                           self.investment.ticker,
                                           self.account)


from django.contrib import admin

admin.site.register(Portfolio)
admin.site.register(Account)
admin.site.register(Investment)
admin.site.register(Position)

